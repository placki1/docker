Wymagania:  
&nbsp;- docker  
&nbsp;- docker-compose  

Projekt uruchamia się za pomocą docker-compose.  
Aby go uruchomić należy pobrać docker-compose.yml znajdujący się w compose i wywołać komendę docker-compose up w miejscu do którego pobraliśmy docker-compose.yml  

Dokcer compose:   
&nbsp;   docker-compose up  

Aby aplikacja się uruchomiła porty 3000 i 8081 muszą być zwolnione  

Stronę główną można wyświetlić na localhost:3000  
